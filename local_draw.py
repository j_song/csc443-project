import sys
import leveldb
import networkx as nx
import matplotlib.pyplot as plt
import Queue

actor_movie = leveldb.LevelDB('./actor_movie')
movie_actor = leveldb.LevelDB('./movie_actor')
color_dict = {0: 'r', 1: 'b', 2: 'g', 3: 'y', 4: 'c', 5: 'w', 6: 'k', 7: 'm'}

def restricted_BFS(G, start, max_d, max_b, width, colors):
    to_ex = Queue.Queue()
    to_ex.put((start, 0))
    explored = set()
    explored.add(start)
    colors[start] = color_dict[0]

    while not to_ex.empty():
        now = to_ex.get()
        actor = now[0]
        d = now[1]
        # exceed maximum distance from start actor
        if d >= max_d:
            break

        try:
            movie_val = actor_movie.Get(actor)
            movies = movie_val.split('\n')
            neighbors = set()
        
            for m in movies:
                try:
                    actor_val = movie_actor.Get(m)
                    actors = actor_val.split('\n')
                    neighbors = neighbors.union(set(actors))
                except KeyError:
                    pass

            # identify top-k collaborators   
            count_dict = {}
            for nb in neighbors:
                try:
                    movie_val = actor_movie.Get(nb)
                    nb_movies = movie_val.split('\n')
                    common = set(movies).intersection(set(nb_movies))
                    if len(common) in count_dict:
                        count_dict[len(common)].append(nb)
                    else:
                        count_dict[len(common)] = [nb]
                except KeyError:
                    pass

            keys = count_dict.keys()
            keys.sort(reverse=True)
            idx = 0
            k = max_b
            while idx < len(keys):
                actors = count_dict[keys[idx]]
                try:
                    actors.remove(actor)
                except:
                    pass

                for a in actors:
                    if k == 0:
                        break
                    else:
                        G.add_edge(unicode(actor), unicode(a))
                        width.append(keys[idx])
                        k -= 1
                        if a not in explored:
                            to_ex.put((a, d+1))
                            explored.add(a)
                            colors[a] = color_dict[d+1]
                            
                if k == 0:
                    break
                else:
                    idx += 1
        except:
            pass
            
            
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: python local_draw.py actor max_d (max_b)"
        sys.exit(0)

        
    actor = sys.argv[1]
    max_d = int(sys.argv[2])
    
    if len(sys.argv) == 4:
        max_branch = int(sys.argv[3])
    else:
        max_branch = 5

    fig, ax = plt.subplots()
    G = nx.Graph()
    width = []
    colors = {}
    restricted_BFS(G, actor, max_d, max_branch, width, colors)
    
    #print nx.info(G)
    #print G.nodes()

    nx.draw_spring(G, width=width, node_color=colors.values(), alpha=0.5, node_size=100, font_size=10)
    ax.set_title('%s local network, max_d=%d, max_b=%d' %(actor, max_d, max_branch))
    plt.show()
