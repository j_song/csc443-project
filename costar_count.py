import leveldb
import sys
import numpy as np
import matplotlib.pyplot as plt
import pickle

if __name__ == "__main__":
    #actor_movie = leveldb.LevelDB('./actor_movie')
    #movie_actor = leveldb.LevelDB('./movie_actor')
    output = open('costar.pkl', 'rb')
    #costar_count = {}
    #actor_iter = actor_movie.RangeIter()
    
    # while 1:
    #     try:
    #         k, v = actor_iter.next()
    #         movies = v.split('\n')
    #         costars = set()

    #         for m in movies:
    #             if len(m) > 0:
    #                 try:
    #                     val = movie_actor.Get(m)
    #                     actors = val.split('\n')
                        
    #                     for a in actors:
    #                         if len(a) > 0 and a != k:
    #                             costars.add(a)
    #                 except KeyError:
    #                     pass

    #         if len(costars) in costar_count:
    #             costar_count[len(costars)].append(k)
    #         else:
    #             costar_count[len(costars)] = [k]
    #     except StopIteration:
    #         break

    #pickle.dump(costar_count, output)
    #output.close()

    costar_count = pickle.load(output)

    reverse = {}
    for k, v in costar_count.items():
        for a in v:
            reverse[a] = k

    out = open('reverse.pkl', 'wb')
    pickle.dump(reverse, out)
    out.close()

    # fig, ax = plt.subplots()
    # N = len(costar_count.keys())
    # keys = costar_count.keys()
    # keys.sort()
    # count = [len(costar_count[t]) for t in keys]
    # ind = np.arange(N)
    # width = 1
    # rects1 = ax.bar(ind, count, 0.5 * width, color='r')
    # ax.set_ylabel('Number of actors')
    # ax.set_title('Number of actors vs Number of costars')
    # #ax.set_xticks(range(0, len(count), 10))
    # #ax.set_xticklabels(range(0, len(count), 10), size='xx-small')
    
    
    # # for i in range(10):
    #  #    ctr = keys[-1-i]
        
    #  #    print "Costar Count: %d" %ctr
    #  #    for a in costar_count[ctr]:
    #  #        print a
        
    #  #    print '\n\n'
        
    
    # plt.show()
