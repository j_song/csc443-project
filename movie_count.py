import sys
import leveldb
import matplotlib.pyplot as plt

if __name__ == "__main__":
    actor_movie = leveldb.LevelDB('./actor_movie')
    movie_ctr = {}
    
    actor_iters = actor_movie.RangeIter()

    while 1:
        try:
            k, v = actor_iters.next()
            movies = v.split('\n')
            if len(movies) in movie_ctr:
                movie_ctr[len(movies)] += 1
            else:
                movie_ctr[len(movies)] = 1
        except StopIteration:
            break

    
    fig, ax = plt.subplots()
    N = len(movie_ctr.keys())
    keys = movie_ctr.keys()
    keys.sort()
    count = [movie_ctr[k] for k in keys]
    ind = range(N)
    width = 1
    rects = ax.bar(ind, count, width, color='r')
    ax.set_ylabel('Number of actors')
    ax.set_xlabel('Number of movies')
    ax.set_title('Number of actors vs Number of movies')
    ax.set_xticks(range(0, len(count), 10))
    ax.set_xticklabels(range(0, len(count), 10), size='xx-small')
    plt.show()
