import leveldb
import matplotlib.pyplot as plt

if __name__ == "__main__":
    movie_actor = leveldb.LevelDB('./movie_actor')
    year = {}
    
    movie_iters = movie_actor.RangeIter()

    while 1:
        try:
            k, v = movie_iters.next()
            items = k.split()

            for item in items:
                if item.startswith('('):
                    try:
                        sub_str = item[1:5]
                        y = int(sub_str)
                        if 1900 < y < 2015:
                            if y in year:
                                year[y] += 1
                            else:
                                year[y] = 1
                                break
                    except:
                        pass
        except StopIteration:
            break

    fig, ax = plt.subplots()
    keys = year.keys()
    keys.sort()
    movie_ctrs = [year[k] for k in keys]
    N = len(keys)
    ind = keys
    width = 1
    rects = ax.bar(ind, movie_ctrs, width, color='b')
    ax.set_ylabel('Number of movies')
    ax.set_xlabel('Year')
    ax.set_title('Number of movies vs Year')
    ax.set_xticks(range(keys[0], keys[-1], 5))
    ax.set_xticklabels(range(keys[0], keys[-1], 5), size='xx-small')
    plt.show()
