import leveldb
import re
import time

def get_movie_name(line):
    items = line.split()
    try:
        if items[0].startswith('"') or '(TV)' in items or '(V)' in items:
            return ''
    except:
        return ''

    idx = 0
    name = ''
    while idx < len(items) and not items[idx].startswith('('):
        if len(name) == 0:
            name = items[idx]
            idx += 1
        else:
            name += ' ' + items[idx]
            idx += 1

    if idx < len(items):
        name += ' ' + items[idx]
    
    return name
    
if __name__ == "__main__":

    start_time = time.clock()
    actor_file = 'actors.list'
    actress_file = 'actresses.list'
    file_names = [actor_file, actress_file]
    db = leveldb.LevelDB('./actor_movie')
    name_regex = '\S+, \S+( \(\w+\))?'
    reg = re.compile(name_regex)

    for f_name in file_names:
        with open(f_name) as f:
            # slip through the header 
            skip = True
            start = True
            name = ''
            movie_ctr = 0
            movies = ''
            
            line_ctr = 0
            for line in f:

                #print line_ctr
                line_ctr += 1

                # end of contents
                if not skip and line.startswith('-------------------------------------'):
                     break

                line = line.strip()
                items = line.split(None, 1)
                if skip:
                    if len(items) > 1 and items[0] == '----' and items[1] == '------':
                        skip = False
                        continue
                    else:
                        continue

                # end of one actor
                if len(line) == 0:
                    start = True
                    # discard actors who appear in less than 20 movies
                    if movie_ctr < 10:
                        movies = ''
                        name = ''
                        movie_ctr = 0
                        continue
                    else:
                        db.Put(name, movies)
                        movies = ''
                        name = ''
                        movie_ctr = 0

                
                # start of a new actor
                if len(name) == 0:
                    result = reg.match(line)
                    if result:
                        start = result.start()
                        end = result.end()
                        name = line[start : end]
                        line = line[end :]
                        movie_name = get_movie_name(line)
                        
                        if movie_name:
                            movies += movie_name
                            movie_ctr += 1
                else:
                    movie_name = get_movie_name(line)

                    if movie_name:
                        movie_ctr += 1
                        movies += '\n' + movie_name
                        
    # reverse dictionary
    movie_db = leveldb.LevelDB('./movie_actor')
    
    iters = db.RangeIter()

    while 1:
        try:
            k, v = iters.next()
            movies = v.split('\n')

            for movie in movies:
                try:
                    actors = movie_db.Get(movie)
                    actors += '\n' + k
                    movie_db.Put(movie, actors)
                except KeyError:
                    actors = k
                    movie_db.Put(movie, actors)

        except StopIteration:
            break

    print "TOTAL TIME: " + str(time.clock() - start_time)

