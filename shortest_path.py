import sys
import leveldb
import Queue

actor_movie = leveldb.LevelDB('./actor_movie')
movie_actor = leveldb.LevelDB('./movie_actor')

def BFS(start, end):
    to_ex = Queue.Queue()
    to_ex.put(start)
    explored = set()
    explored.add(start)
    
    # path consists of a list of movies that connects start and end
    paths = {}
    paths[start] = []

    while not to_ex.empty():
        now_ex = to_ex.get()
        try:
            movie = actor_movie.Get(now_ex)
            movies = movie.split('\n')
            
            for m in movies:
                if len(m) > 0:
                    try:
                        actor = movie_actor.Get(m)
                        actors = actor.split('\n')
                        for a in actors:
                            if len(a) > 0:
                                if a == end:
                                    return paths[now_ex] + [m + ' ' + a]
                                
                                if not a in explored:
                                    to_ex.put(a)
                                    explored.add(a)
                                    paths[a] = paths[now_ex] + [m + ' ' + a]
                    except KeyError:
                        pass
        except KeyError:
            pass
    return []

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Usage: python shortest_path.py actor1 actor2"
        sys.exit(0)

    actor1 = sys.argv[1]
    actor2 = sys.argv[2]
    path = BFS(actor1, actor2)
    
    if len(path) == 0:
        print "No path found"
    else:
        print "Path length: %d" %(len(path))
        for m in path:
            print m
